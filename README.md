# gsupersheeps

#### 介绍
supersheeps服务器压力测试工具使用的golang插件，支持linux和windows，可以使用golang作为项目开发语言。linux下使用cgo，编译需要gcc环境，windows下使用syscall。


#### 安装

go get gitee.com/lutianming/gsupersheeps

#### 代码示例


```go
//main.go
package main

import (
	sheeps "gitee.com/lutianming/gsupersheeps"
)

type User struct {
	sheeps.BaseUser
	tcpsock uintptr
}

func (U *User) EventStart(taskcfg *sheeps.TaskConfig, user_number int) {
	U.Log(2, "EventStart")
}

func (U *User) EventConnectOpen(ip string, port int, protocol int) {
	U.Log(2, "EventConnectOpen")
	U.tcpsock = U.SocketConnect(ip, port, protocol)
	U.PlayPause()
}

func (U *User) EventConnectMade(hsock uintptr) {
	U.Log(2, "EventConnectMade")
	U.PlayNoraml()
}

func (U *User) EventConnectFailed(hsock uintptr, errno int) {
}

func (U *User) EventConnectSend(ip string, port int, data []byte, protocol int) {
	U.Log(2, "EventConnectSend:"+string(data))
	U.SocketSend(U.tcpsock, data, len(data))
	U.PlayPause()
}

func (U *User) EventConnectRecved(hsock uintptr, data []byte) {
	U.Log(2, "EventConnectRecved:"+string(data))
	U.PlayNoraml()
}

func (U *User) EventConnectClose(ip string, port int, protocol int) {
}

func (U *User) EventConnectClosed(hsock uintptr, errno int) {
}

func (U *User) EventTimeOut() {
}

func (U *User) EventStop(err string) {
	U.Log(2, "EventStop:"+err)
}

func create_user(user_handle uintptr) sheeps.BaseUserCall {
	user := new(User)
	return user
}

func main() {
	sheeps.LoadEngin(0, 0, true, "", create_user)
}
```

#### 使用

1.安装golang环境
2.创建项目文件夹，如project  
3.在project下创建main.go,拷贝示例代码  
4.下载supersheeps最新版本，解压动态库Sheeps.dll/libsheeps.so和sheeps.ini到project目录  
5.依次执行命令 
``` 
	go mod init test  
	go get gitee.com/lutianming/gsupersheeps  
	go mod vendor  
	go build
```
6.启动可执行程序 test

#### Q&A

1.vscode调试golang代码时，调试控制台看不到日志输出
答：在launch.json添加配置 "console": "integratedTerminal"
```
{
    "version": "0.2.0",
    "configurations": [
    
        {
            "name": "debug go",
            "type": "go",
            "request": "launch",
            "mode": "auto",
            "program": "${fileDirname}",
            "console": "integratedTerminal"
        }
    ]
}
```
